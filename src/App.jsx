import { Routes, Route } from "react-router-dom";
import { RecoilRoot } from "recoil";

import "asset/sass/App.scss";
import Layout from "pages/Layout";
import Home from "pages/Home";
import CraftingProfit from "pages/CraftingProfit/Index";
import CraftingProfitUpdatePrices from "pages/CraftingProfit/UpdatePrices";
import Dailies from "pages/Dailies";

export default function App() {
  return (
    <RecoilRoot>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="crafting-profit" element={<CraftingProfit />} />
          <Route
            path="crafting-profit/update-prices"
            element={<CraftingProfitUpdatePrices />}
          />
          <Route path="dailies" element={<Dailies />} />
        </Route>
      </Routes>
    </RecoilRoot>
  );
}
