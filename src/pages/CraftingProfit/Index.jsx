import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";

import state from "lib/state";

import { showAeModal } from "lib/hooks";

import AeItemModal from "component/modal/AeItem";
import CraftingProfitTable from "component/table/CraftingProfit";

import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

export default function Index() {
  let localState = state.buildLocal(useRecoilState, {
    component: { aeItem: ["item", "show", "mats"] },
  });
  const stateRel = {
    aeItem: localState.component.aeItem,
  };
  return (
    <Row>
      <Container className="page-title">
        <h1>Crafing Profit</h1>
      </Container>

      <Container className="action-buttons">
        <Button
          onClick={() => {
            showAeModal(
              stateRel.aeItem,
              {
                is_material: false,
                bundle_size: 1,
                price: 0,
                stronghold_craft_cost: 5,
              },
              []
            );
          }}
          variant="latoolsDarkBlue"
        >
          Add Item
        </Button>
        <Button
          onClick={() => {
            showAeModal(
              stateRel.aeItem,
              { is_material: true, stronghold_craft_cost: 0, price: 0 },
              []
            );
          }}
          variant="latoolsDarkBlue"
        >
          Add Material
        </Button>
        <Link
          to="/crafting-profit/update-prices"
          className="btn btn-latoolsDarkBlue"
        >
          Manage Items
        </Link>
      </Container>
      <Container>
        <CraftingProfitTable />
        <AeItemModal />
      </Container>
    </Row>
  );
}
