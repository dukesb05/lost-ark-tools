import { useRecoilState } from "recoil";

import state from "lib/state";
import { showAeModal } from "lib/hooks";
import itemsMatsModel from "lib/models/itemsMats";

import AeItemModal from "component/modal/AeItem";
import NumberFieldGrid from "component/grid/NumberField";

import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

export default function UpdatePrices() {
  let localState = state.buildLocal(useRecoilState, {
    page: ["updatePrices"],
    component: { aeItem: ["item", "show", "mats"] },
  });
  const stateRel = {
    updates: localState.page.updatePrices.updates,
    delModalShow: localState.page.updatePrices.delModalShow,
    delModalItem: localState.page.updatePrices.delModalItem,
    reloadItems: localState.page.updatePrices.forceItemsReload,
    aeItem: localState.component.aeItem,
  };

  const items = state.getItems(
    "both",
    "name",
    "asc",
    stateRel.reloadItems.data
  );
  if (items === null) {
    return <Container>Loading...</Container>;
  }

  const updatePrice = (price, item) =>
    stateRel.updates.setData((prevState) => {
      return { ...prevState, ...{ [item._id]: price } };
    });
  const savePrices = () => {
    for (const [item_id, price] of Object.entries(stateRel.updates.data)) {
      for (let item of [...items.items, ...items.mats]) {
        if (item._id === item_id) {
          item.price = price;
          item.update();
          break;
        }
      }
    }
    stateRel.updates.setData({});
  };

  const handleDeleteBtn = (index, item) => {
    stateRel.delModalShow.setData(true);
    stateRel.delModalItem.setData(item);
  };

  const handleCloseDelModal = () => {
    stateRel.delModalShow.setData(false);
    stateRel.delModalItem.setData({});
  };
  const deleteItem = () => {
    itemsMatsModel
      .deleteBy({ _item_id: stateRel.delModalItem.data._id })
      .then(() => {
        stateRel.delModalItem.data.delete().then(() => {
          stateRel.delModalShow.setData(false);
          stateRel.delModalItem.setData({});
          stateRel.reloadItems.setData(stateRel.reloadItems.data + 1);
        });
      });
  };

  return (
    <Row>
      <Container className="page-title">
        <h1>Update Prices</h1>
      </Container>
      <Container className="prices-cont">
        {Object.keys(stateRel.updates.data).length > 0 && (
          <Button
            onClick={savePrices}
            variant="latoolsDarkBlue"
            className="float-end"
          >
            Save Prices
          </Button>
        )}
        <Tabs defaultActiveKey="items" className="mb-3" variant="pills">
          <Tab eventKey="items" title="Items">
            <NumberFieldGrid
              items={items.items}
              chunkSize={4}
              onNumberChange={updatePrice}
              icon={"goldIcon"}
              showDelBtn={true}
              onDelete={handleDeleteBtn}
              newBtn={
                <Button
                  variant="latoolsBlue"
                  onClick={() => {
                    showAeModal(stateRel.aeItem, { is_material: false }, []);
                  }}
                >
                  +
                </Button>
              }
            />
          </Tab>
          <Tab eventKey="materials" title="Materials">
            <NumberFieldGrid
              items={items.mats}
              chunkSize={4}
              onNumberChange={updatePrice}
              showDelBtn={true}
              onDelete={handleDeleteBtn}
              icon={"goldIcon"}
              newBtn={
                <Button
                  variant="latoolsBlue"
                  onClick={() => {
                    showAeModal(stateRel.aeItem, { is_material: true }, []);
                  }}
                >
                  +
                </Button>
              }
            />
          </Tab>
        </Tabs>
      </Container>
      <AeItemModal />
      <Modal
        show={stateRel.delModalShow.data}
        onHide={handleCloseDelModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Body>
          Are you sure you want to delete {stateRel.delModalItem.data.name}?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="latoolsGrey" onClick={handleCloseDelModal}>
            Cancel
          </Button>
          <Button variant="latoolsRed" onClick={deleteItem}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  );
}
