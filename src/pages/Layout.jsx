import { useState } from "react";
import { Link, Outlet } from "react-router-dom";

import itemsModel from "lib/models/items";
import itemsMatsModel from "lib/models/itemsMats";
import settingsModel from "lib/models/settings";
import state from "lib/state";

import itemsData from "data/items";
import itemsMatsData from "data/itemsMats";

import Alert from "react-bootstrap/Alert";
import Container from "react-bootstrap/Container";
import Modal from "react-bootstrap/Modal";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

export default function Layout() {
  const isSetup = state.isSetup();
  const [showSetupModal, setShowSetupModal] = useState(false);
  const [setupModalMsg, setSetupModalMsg] = useState(
    "Setting up inital data..."
  );

  const handleClose = () => setShowSetupModal(false);
  const runSetup = () => {
    setShowSetupModal(true);
    setSetupModalMsg(
      "Saving data for craftable items and gathering materials..."
    );
    itemsModel.bulkInsert(itemsData).then(
      () => {
        setSetupModalMsg("Saving materials required to craft each item...");
        itemsMatsModel.bulkInsert(itemsMatsData).then(
          () => {
            setSetupModalMsg("Finalizing Setup...");
            settingsModel.inst().then((settings) => {
              settings.db_version = 1.0;
              settings.update().then((res) => {
                setSetupModalMsg("Setup Finished!");
                setShowSetupModal(false);
                window.location.reload();
              });
            });
          },
          (err) => {
            console.log(err);
            setSetupModalMsg(
              "An error occured saving the materials required to craft each item. Please refresh the page and try again."
            );
          }
        );
      },
      (err) => {
        console.log(err);
        setSetupModalMsg(
          "An error occured saving the data for craftable items and gathering matherials. Please refresh the page and try again."
        );
      }
    );
  };
  return (
    <div className="App">
      <header className="header">
        <Navbar variant="dark" expand="lg" sticky="top">
          <Container>
            <Link to="/" className="navbar-brand">
              Lost Ark Tools
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Link to="/crafting-profit" className="nav-link">
                  Crafting Profit
                </Link>
                <Link to="/dailies" className="nav-link">
                  Daily Tasks
                </Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>
      <Container className="body">
        {isSetup ? (
          <Outlet />
        ) : (
          <div>
            <Alert
              className="setup-alert"
              onClick={() => {
                runSetup();
              }}
            >
              Looks like this is your first time using Lost Ark Tools! Please
              click this message to set up some initial data in your local
              browser storage.
            </Alert>
            <Modal
              show={showSetupModal}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Body>
                <div className="loadingIcon">
                  <div></div>
                  <div></div>
                </div>
                <div className="setup-msg">{setupModalMsg}</div>
              </Modal.Body>
            </Modal>
          </div>
        )}
      </Container>
    </div>
  );
}
