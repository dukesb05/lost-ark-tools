import { Link } from "react-router-dom";

import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function Home() {
  return (
    <Row>
      <Link to="/crafting-profit" className="nav-link">
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>Crafting Profit</Card.Title>
              <Card.Text>
                Shows profit per tradable crafted item from stronghold.
                Information is only as accurate as market prices so check and
                update those before buying/crafting.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Link>
    </Row>
  );
}
