import { useEffect, useState } from "react";
import { useRecoilState } from "recoil";

import state from "lib/state";

import Button from "react-bootstrap/Button";

export default function NumberField(props) {
  let localState = state.buildLocal(useRecoilState, {
    component: ["numberField"],
  });
  const [value, setValue] = useState(0);
  const stateRel = {
    interval: localState.component.numberField.interval,
    sliderMouseDown: localState.component.numberField.sliderMouseDown,
    sliderX: localState.component.numberField.sliderX,
  };
  const incDecStep = props.incDecStep ? props.incDecStep : 1;
  const incDecVal = (dir) => {
    dir === "inc"
      ? setValue((oldValue) => oldValue + incDecStep)
      : setValue((oldValue) =>
          oldValue - incDecStep < 0 ? 0 : oldValue - incDecStep
        );
  };
  const onIncBtnMouseDown = () => {
    incDecVal("inc");
    stateRel.interval.setData(setInterval(incDecVal, 150, "inc"));
    stateRel.sliderMouseDown.setData(false);
  };
  const onIncBtnMouseUp = () => {
    stateRel.sliderMouseDown.setData(false);
    clearInterval(stateRel.interval.data);
    stateRel.interval.setData(null);
    fireValueChange();
  };
  const onDecBtnMouseDown = () => {
    incDecVal("dec");
    stateRel.interval.setData(setInterval(incDecVal, 150, "dec"));
    stateRel.sliderMouseDown.setData(false);
  };
  const onDecBtnMouseUp = () => {
    stateRel.sliderMouseDown.setData(false);
    clearInterval(stateRel.interval.data);
    stateRel.interval.setData(null);
    fireValueChange();
  };
  const onSliderMouseMove = (e) => {
    if (stateRel.sliderMouseDown.data) {
      let newVal =
        parseInt(value) +
        (e.clientX - stateRel.sliderX.data) * (incDecStep ? incDecStep : 5);
      setValue(newVal >= 0 ? newVal : 0);
      stateRel.sliderX.setData(e.clientX);
    }
    stopProp(e);
  };
  const onSliderMouseDown = (e) => {
    stateRel.sliderX.setData(e.clientX);
    stateRel.sliderMouseDown.setData(true);
    stopProp(e);
  };
  const onSliderMouseUp = (e) => {
    stateRel.sliderMouseDown.setData(false);
    fireValueChange();
    stopProp(e);
  };
  const stopProp = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const fireValueChange = () => {
    if (typeof props.onChange === "function") {
      props.onChange(value, props.options);
    }
  };
  useEffect(() => {
    setValue(props.defaultValue);
  }, [props.defaultValue]);
  return (
    <div className="number-field">
      <Button
        className="minus"
        onMouseDown={onDecBtnMouseDown}
        onMouseUp={onDecBtnMouseUp}
      >
        -
      </Button>
      <div
        className="field"
        onMouseMove={onSliderMouseMove}
        onMouseDown={onSliderMouseDown}
        onMouseUp={onSliderMouseUp}
      >
        {value}
        {props.icon && <i className={props.icon} />}
      </div>
      <Button
        className="plus"
        onMouseDown={onIncBtnMouseDown}
        onMouseUp={onIncBtnMouseUp}
      >
        +
      </Button>
    </div>
  );
}
