import React from "react";
import { useRecoilState } from "recoil";

import state from "lib/state";

import { showAeModal } from "lib/hooks";

import CraftingCostTooltip from "component/tooltip/CraftingCost";

import Container from "react-bootstrap/Container";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Table from "react-bootstrap/Table";

export default function CraftingProfitTable(componentProps) {
  let localState = state.buildLocal(useRecoilState, {
    component: { aeItem: ["item", "show", "mats"] },
    page: { craftingProfit: ["forceItemsReload"] },
  });
  const stateRel = {
    aeItem: localState.component.aeItem,
    reloadItems: localState.page.craftingProfit.forceItemsReload,
  };
  const tableData = state.getItems("items", "profit", "desc", stateRel.reloadItems.data);
  if (tableData === null) {
    return <Container>Loading...</Container>;
  }
  return (
    <Table className="items-profit" variant="latoolsBlue">
      <thead>
        <tr>
          <th>Item</th>
          <th>Market Value</th>
          <th>Crafting Cost</th>
          <th>Profit</th>
        </tr>
      </thead>
      <tbody>
        {tableData.map((item, i) => (
          <tr key={i}>
            <td
              className="item-name"
              onClick={() => {
                showAeModal(
                  stateRel.aeItem,
                  item.toJSON(),
                  Object.values(item.mats)
                );
              }}
            >
              {item.name}
            </td>
            <td>
              {item.price}
              <i className="goldIcon" />
            </td>
            <OverlayTrigger
              placement={i > 15 ? "top" : "bottom"}
              overlay={CraftingCostTooltip(item)}
            >
              <td>
                {item.crafting_cost}
                <i className="goldIcon" />
              </td>
            </OverlayTrigger>
            <td>
              {item.profit}
              <i className="goldIcon" />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}
