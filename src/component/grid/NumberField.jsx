import { useEffect, useState } from "react";

import NumberField from "component/field/Number";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function NumberFieldGrid(props) {
  const [items, setItems] = useState([]);
  const onNumberChange = (value, options) => {
    if (typeof props.onNumberChange === "function") {
      props.onNumberChange(value, options);
    }
  };
  const buildChunks = (items, chunkSize) => {
    let chunks = [];
    for (let i = 0; i < items.length; i += chunkSize) {
      chunks.push(items.slice(i, i + chunkSize));
    }
    return chunks;
  };
  const headerText = (index, item) => {
    if (typeof props.headerText === "function") {
      return props.headerText(index, item);
    }
    return item.name;
  };
  const handleDelBtn = (index, item) => {
    if (typeof props.onDelete === "function") {
      return props.onDelete(index, item);
    }
  };
  const getIndex = (col, row) => {
    return col + props.chunkSize * row;
  };
  const cardBodyClass = (index, item) => {
    if (typeof props.cardBodyClass === "function") {
      return props.cardBodyClass(index, item);
    }
    return "";
  };
  const icon = props.hasOwnProperty("icon") ? props.icon : false;
  const chunks = buildChunks(items, props.chunkSize);
  const newBtnCol = props.newBtn ? (
    <Col lg={12 / props.chunkSize} className="col new-btn">
      {props.newBtn}
    </Col>
  ) : (
    false
  );
  const valueField = props.valueField ? props.valueField : "price";
  useEffect(() => {
    setItems(props.items);
  }, [props.items]);
  return (
    <div className="number-field-grid">
      {!chunks.length ? (
        <Row>{newBtnCol}</Row>
      ) : (
        chunks.map((chunk, j) => (
          <Row key={j}>
            {chunk.map((item, i) => (
              <Col key={i} lg={12 / props.chunkSize} className="col">
                <Card>
                  <Card.Header>
                    {headerText(getIndex(i, j), item)}
                    {props.showDelBtn && (
                      <Button
                        variant="latoolsRed"
                        className="float-end"
                        onClick={() => handleDelBtn(getIndex(i, j), item)}
                      >
                        <i className="trashIcon"></i>
                      </Button>
                    )}
                  </Card.Header>
                  <Card.Body className={cardBodyClass(getIndex(i, j), item)}>
                    <NumberField
                      options={{
                        _id: item._id,
                        index: getIndex(i, j),
                      }}
                      key={`price_${i}`}
                      defaultValue={item[valueField]}
                      icon={icon}
                      onChange={onNumberChange}
                    />
                  </Card.Body>
                </Card>
              </Col>
            ))}
            {j === chunks.length - 1 &&
              (chunk.length === props.chunkSize && newBtnCol ? (
                <Row>{newBtnCol}</Row>
              ) : (
                newBtnCol
              ))}
          </Row>
        ))
      )}
    </div>
  );
}
