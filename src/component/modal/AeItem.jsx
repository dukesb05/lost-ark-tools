import { useRecoilState } from "recoil";

import categories from "lib/categories";
import state from "lib/state";
import itemsModel from "lib/models/items";
import itemsMatsModel from "lib/models/itemsMats";

import NumberField from "component/field/Number";

import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import InputGroup from "react-bootstrap/InputGroup";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import NumberFieldGrid from "component/grid/NumberField";

export default function AeItem(componentProps) {
  let localState = state.buildLocal(useRecoilState, {
    component: ["aeItem"],
    page: { craftingProfit: ["forceItemsReload"] },
  });
  const stateRel = {
    show: localState.component.aeItem.show,
    submitting: localState.component.aeItem.submitting,
    item: localState.component.aeItem.item,
    mats: localState.component.aeItem.mats,
    errors: localState.component.aeItem.errors,
    errMsg: localState.component.aeItem.errMsg,
    fieldsChecked: localState.component.aeItem.fieldsChecked,
    saveSuccessful: localState.component.aeItem.saveSuccessful,
    reloadItems: localState.page.craftingProfit.forceItemsReload,
  };
  const handleClose = () => {
    stateRel.errors.setData(null);
    stateRel.errMsg.setData({});
    stateRel.fieldsChecked.setData([]);
    stateRel.show.setData(false);
    stateRel.saveSuccessful.setData(false);
  };
  const updateField = (field, value) =>
    stateRel.item.setData((prevState) => {
      let item = JSON.parse(JSON.stringify(stateRel.item.data));
      item[field] = value;
      return { ...prevState, ...item };
    });
  const onFieldChange = (e, options = {}) => {
    const field = typeof e === "object" ? e.target.name : options.field;
    switch (field) {
      case "cat":
        const [cat, sub_cat] = parseCatKey(e.target.value);
        updateField("cat", cat);
        updateField("sub_cat", sub_cat);
        break;
      case "stronghold_craft_cost":
        updateField("stronghold_craft_cost", e);
        break;
      case "price":
        updateField("price", e);
        break;
      default:
        updateField(field, e.target.value);
    }
  };
  const parseCatKey = (key) => {
    return key.split(":::");
  };
  const getCatKey = (cat, subCat) => cat + ":::" + subCat;
  const updateMatField = (field, value, index) => {
    let mats = JSON.parse(JSON.stringify(stateRel.mats.data));
    mats[index][field] = value;
    stateRel.mats.setData(mats);
  };
  const removeMat = (index) => {
    let mats = JSON.parse(JSON.stringify(stateRel.mats.data));
    mats.splice(index, 1);
    stateRel.mats.setData(mats);
  };
  const numberFieldChange = (value, options) => {
    updateMatField("qty", value, options.index);
  };
  const saveItem = () => {
    stateRel.submitting.setData(true);
    const [item, mats, errors, errMsg, fieldsChecked] = validate();

    if (errors.length > 0) {
      stateRel.errors.setData(errors);
      stateRel.errMsg.setData(errMsg);
      stateRel.fieldsChecked.setData(fieldsChecked);
      stateRel.submitting.setData(false);
      stateRel.saveSuccessful.setData(false);
    } else {
      item.save().then(
        () => {
          const reloadNum = stateRel.reloadItems.data + 1;
          if (!item.is_material) {
            itemsMatsModel.deleteBy({ _item_id: item._id }).then(() => {
              itemsMatsModel
                .bulkInsert(mats.map((v) => ({ ...v, _item_id: item._id })))
                .then(() => {
                  stateRel.errors.setData(null);
                  stateRel.errMsg.setData({});
                  stateRel.fieldsChecked.setData([]);
                  stateRel.submitting.setData(false);
                  stateRel.saveSuccessful.setData(true);
                  stateRel.reloadItems.setData(reloadNum);
                });
            });
          } else {
            stateRel.errors.setData(null);
            stateRel.errMsg.setData({});
            stateRel.fieldsChecked.setData([]);
            stateRel.submitting.setData(false);
            stateRel.saveSuccessful.setData(true);
            stateRel.reloadItems.setData(reloadNum);
          }
        },
        () => {
          console.log("wtf happened here");
        }
      );
    }
  };
  const validate = () => {
    const [item, itemErrors, itemErrMsg, itemFieldsChecked] = validateItem();
    let mats = [];
    let matsErrors = [];
    let matsErrMsg = {};
    let matsFieldsChecked = [];
    if (!item.is_material) {
      [mats, matsErrors, matsErrMsg, matsFieldsChecked] = validateMats();
    }
    return [
      item,
      mats,
      [...itemErrors, ...matsErrors],
      { ...itemErrMsg, ...matsErrMsg },
      [...itemFieldsChecked, ...matsFieldsChecked],
    ];
  };

  const validateItem = () => {
    let errors = [];
    let errMsg = {};
    let item = new itemsModel(JSON.parse(JSON.stringify(stateRel.item.data)));

    if (!item.name) {
      errors.push("name");
      errMsg["name"] = `Enter a name \n`;
    }
    if (!item.cat) {
      errors.push("cat");
      errMsg["cat"] = `Select a cat \n`;
    }
    if (item.is_material && !item.bundle_size) {
      errors.push("bundle_size");
      errMsg["bundle_size"] = `Select a bundle size \n`;
    }
    return [
      item,
      errors,
      errMsg,
      [
        "name",
        "cat",
        "sub_cat",
        "is_material",
        "bundle_size",
        "stronghold_craft_cost",
        "price",
      ],
    ];
  };
  const validateMats = () => {
    let errors = [];
    let errMsg = {};
    let fieldsChecked = [];

    let mats = [];
    if (stateRel.mats.data.length > 0) {
      stateRel.mats.data.forEach((mat, index) => {
        const newMat = {
          _mat_id: mat._id,
          qty: mat.qty,
        };
        mats.push(newMat);
        if (!newMat._mat_id && !newMat.qty) {
          errors.push("mainMats");
          errMsg["mainMats"] =
            "Either remove or fill in the empty materials \n";
        }
        if (!newMat._mat_id) {
          errors.push("_mat_id_" + index);
          errMsg[
            "_mat_id_" + index
          ] = `Each material dropdown should have something selected \n`;
        }
        if (!newMat.qty) {
          errors.push("_mat_qty_" + index);
          errMsg["_mat_qty_" + index] = `Each material qty should be > 0 \n`;
        }
        fieldsChecked.push("_mat_id_" + index);
        fieldsChecked.push("_mat_qty_" + index);
      });
    } else {
      errors.push("mainMats");
      errMsg["mainMats"] = "Please add at least one material \n";
    }
    return [mats, errors, errMsg, fieldsChecked];
  };
  const addMaterial = () => {
    stateRel.mats.setData((oldMats) => [
      ...oldMats,
      {
        _id: "",
        qty: 0,
      },
    ]);
  };
  const checkForError = (field, isValid) => {
    return !stateRel.fieldsChecked.data.includes(field)
      ? undefined
      : isValid
      ? !stateRel.errors.data.includes(field)
      : stateRel.errors.data.includes(field);
  };
  const matQtyFieldValidClass = (index) => {
    const isValid = checkForError("_mat_qty_" + index, true);
    if (isValid === undefined) {
      return "";
    }
    return isValid ? "is-valid" : "is-invalid";
  };

  const tableData = state.getItems("both");
  if (tableData === null) {
    return <Container>Loading...</Container>;
  }
  const MaterialDropdown = (index, mat) => {
    return (
      <>
        <Form.Select
          aria-label="Material"
          value={mat._id}
          onChange={(e) => {
            updateMatField("_id", e.target.value, index);
          }}
          size="sm"
          className="mat-fld float-start"
          isValid={checkForError("_mat_id_" + index, true)}
          isInvalid={checkForError("_mat_id_" + index, false)}
        >
          <option>Select a Material</option>
          {tableData.mats.map((tableMat) =>
            !stateRel.mats.data.map((a) => a._id).includes(tableMat._id) ||
            tableMat._id === mat._id ? (
              <option value={tableMat._id} key={tableMat._id}>
                {tableMat.name}
              </option>
            ) : (
              ""
            )
          )}
        </Form.Select>
        <Button
          variant="latoolsRed"
          className="float-end"
          onClick={() => removeMat(index)}
        >
          <i className="trashIcon"></i>
        </Button>
      </>
    );
  };
  return (
    <Modal
      show={stateRel.show.data}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      size="xl"
    >
      <Modal.Header closeButton closeVariant="white">
        <Modal.Title className="text-center w-100">
          {stateRel.item.data._id
            ? `Update ${stateRel.item.data.name}`
            : `Add ${stateRel.item.data.is_material ? "Material" : "Item"}`}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {stateRel.saveSuccessful.data && (
          <Alert variant="success">{`${
            stateRel.item.data.is_material ? "Material" : "Item"
          } saved successfully!`}</Alert>
        )}
        {!stateRel.saveSuccessful.data && (
          <Form>
            <fieldset disabled={stateRel.submitting.data}>
              <Container>
                <Container className="item-info-cnt">
                  <h4 className="text-center">Item Info</h4>
                  <Row>
                    <Col>
                      <FloatingLabel label="Name" className="mb-3">
                        <Form.Control
                          type="text"
                          placeholder="Name"
                          name="name"
                          defaultValue={stateRel.item.data.name}
                          onChange={onFieldChange}
                          isValid={checkForError("name", true)}
                          isInvalid={checkForError("name", false)}
                        />

                        <Form.Control.Feedback type="invalid">
                          {stateRel.errMsg.data.name}
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>
                    <Col>
                      <FloatingLabel label="Category" className="mb-3">
                        <Form.Select
                          aria-label="Category"
                          defaultValue={getCatKey(
                            stateRel.item.data.cat,
                            stateRel.item.data.sub_cat
                          )}
                          name="cat"
                          onChange={onFieldChange}
                          isValid={checkForError("cat", true)}
                          isInvalid={checkForError("cat", false)}
                        >
                          <option>Select a Category</option>
                          {Object.keys(categories).map((category) => (
                            <optgroup key={category} label={category}>
                              {categories[category].map((sub_cat) => (
                                <option
                                  value={getCatKey(category, sub_cat)}
                                  key={sub_cat}
                                >
                                  {sub_cat}
                                </option>
                              ))}
                            </optgroup>
                          ))}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                          {stateRel.errMsg.data.cat}
                        </Form.Control.Feedback>
                      </FloatingLabel>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <InputGroup
                        className={`mb-3 price-field input-group-number-field${
                          checkForError("price", true) ? " is-valid" : ""
                        }`}
                      >
                        <InputGroup.Text>Price</InputGroup.Text>
                        <NumberField
                          defaultValue={parseInt(stateRel.item.data.price) || 0}
                          icon="goldIcon"
                          onChange={onFieldChange}
                          options={{ field: "price" }}
                        />
                      </InputGroup>
                    </Col>
                    <Col>
                      {stateRel.item.data.is_material ? (
                        <FloatingLabel label="Bundle Size" className="mb-3">
                          <Form.Select
                            aria-label="Bundle Size"
                            defaultValue={stateRel.item.data.bundle_size}
                            name="bundle_size"
                            onChange={onFieldChange}
                            isValid={checkForError("bundle_size", true)}
                            isInvalid={checkForError("cat", false)}
                          >
                            <option>Select Bundle Size</option>
                            <option value={1}>10</option>
                            <option value={100}>100</option>
                          </Form.Select>
                        </FloatingLabel>
                      ) : (
                        <InputGroup
                          className={`mb-3 stronghold-cost-field input-group-number-field${
                            checkForError("stronghold_craft_cost", true)
                              ? " is-valid"
                              : ""
                          }`}
                        >
                          <InputGroup.Text>
                            Stronghold Cost Per Item
                          </InputGroup.Text>
                          <NumberField
                            defaultValue={
                              parseInt(
                                stateRel.item.data.stronghold_craft_cost
                              ) || 5
                            }
                            icon="goldIcon"
                            onChange={onFieldChange}
                            options={{ field: "stronghold_craft_cost" }}
                            incDecStep={5}
                          />
                        </InputGroup>
                      )}
                    </Col>
                  </Row>
                </Container>
                {!stateRel.item.data.is_material && (
                  <Container className="crafting-mats-cnt">
                    <h4 className="text-center">Crafting Mats</h4>
                    {stateRel.errMsg.data.mainMats && (
                      <Alert variant="danger">
                        {stateRel.errMsg.data.mainMats}
                      </Alert>
                    )}
                    <NumberFieldGrid
                      items={stateRel.mats.data}
                      chunkSize={4}
                      headerText={MaterialDropdown}
                      valueField="qty"
                      cardBodyClass={matQtyFieldValidClass}
                      newBtn={
                        typeof stateRel.mats.data === "undefined" ||
                        Object.keys(stateRel.mats.data).length <
                          tableData.mats.length ? (
                          <Button onClick={addMaterial} variant="latoolsBlue">
                            +
                          </Button>
                        ) : (
                          false
                        )
                      }
                      onNumberChange={numberFieldChange}
                    />
                  </Container>
                )}
              </Container>
            </fieldset>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="latoolsGrey"
          onClick={handleClose}
          disabled={stateRel.submitting.data}
        >
          Close
        </Button>
        {!stateRel.saveSuccessful.data && (
          <Button
            onClick={saveItem}
            disabled={stateRel.submitting.data}
            variant="latoolsBlue"
          >
            {stateRel.submitting.data
              ? "Saving..."
              : `${stateRel.item.data._id ? "Update" : "Add"} Item`}
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}
