import Popover from "react-bootstrap/Popover";
import Table from "react-bootstrap/Table";
export default function CraftingCostTooltip(item) {
  return (
    <Popover className="crafting-cost-popover">
      <Popover.Header as="h3">{item.name} Crafting Cost</Popover.Header>
      <Popover.Body>
        <Table bordered size="sm">
          <thead>
            <tr>
              <th>Material</th>
              <th>Bundle Price</th>
              <th>Unit Price</th>
              <th>Qty</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {Object.values(item.mats).map((mat, j) => (
              <tr key={j}>
                <td>{mat.name}</td>
                <td>
                  {mat.price}
                  <i className="goldIcon" /> / {mat.bundle_size}
                </td>
                <td>
                  {mat.unit_price}
                  <i className="goldIcon" />
                </td>
                <td>{mat.qty_per_craft}</td>
                <td>
                  {mat.total_cost}
                  <i className="goldIcon" />
                </td>
              </tr>
            ))}
            <tr>
              <td colSpan={3}>Stronghold Cost</td>
              <td>1</td>
              <td>
                {item.stronghold_craft_cost}
                <i className="goldIcon" />
              </td>
            </tr>
            <tr>
              <th colSpan={3}>Total</th>
              <td colSpan={2}>
                {item.crafting_cost}
                <i className="goldIcon" />
              </td>
            </tr>
          </tbody>
        </Table>
      </Popover.Body>
    </Popover>
  );
}
