var items = [
    {
        _id: "adrophine_potion",
        name: "Adrophine Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Buff",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "awakening_potion",
        name: "Awakening Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Buff",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "camouflage_robe",
        name: "Camouflage Robe",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "campfire",
        name: "Campfire",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "clay_grenade",
        name: "Clay Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "corrosive_bomb",
        name: "Corrosive Bomb",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "dark_grenade",
        name: "Dark Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "destruction_bomb",
        name: "Destruction Bomb",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "electric_grenade",
        name: "Electric Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "elemental_hp_potion",
        name: "Elemental HP Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Recovery",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "flame_grenade",
        name: "Flame Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "flare",
        name: "Flare",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "flash_grenade",
        name: "Flash Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "frost_grenade",
        name: "Frost Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "luterras_horn",
        name: "Luterra's Horn",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "marching_flag",
        name: "Marching Flag",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Buff",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "panacea",
        name: "Panacea",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "pheromone_bomb",
        name: "Peromone Bomb",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "protective_potion",
        name: "Protective Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Buff",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "repair_shop_portal_scroll",
        name: "Repair Shop Portal Scroll",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "sacred_bomb",
        name: "Sacred Bomb",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "sacred_charm",
        name: "Sacred Charm",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "sleep_bomb",
        name: "Sleep Bomb",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "stealth_robe",
        name: "Stealth Robe",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "swiftness_robe",
        name: "Swiftness Robe",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Buff",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "taunting_scarecrow",
        name: "Taunting Scarecrow",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "thunder_potion",
        name: "Thunder Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    },
    {
        _id: "time_stop_potion",
        name: "Time Stop Potion",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Utility",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 10
    },
    {
        _id: "whirlwind_grenade",
        name: "Whirlwind Grenade",
        cat: "Combat Supplies",
        sub_cat: "Battle Item - Offense",
        is_material: false,
        price: 0,
        bundle_size: 1,
        stronghold_craft_cost: 5
    }
];
var mats = [
    {
        _id: "bright_wild_flower",
        name: "Bright Wild Flower",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "crude_mushroom",
        name: "Crude Mushroom",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 100,
        stronghold_craft_cost: 0
    },
    {
        _id: "exquisite_mushroom",
        name: "Exquisite Mushroom",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "fresh_mushroom",
        name: "Fresh Mushroom",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "heavy_iron_ore",
        name: "Heavy Iron Ore",
        cat: "Trader",
        sub_cat: "Mining Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "iron_ore",
        name: "Iron Ore",
        cat: "Trader",
        sub_cat: "Mining Loot",
        is_material: true,
        price: 0,
        bundle_size: 100,
        stronghold_craft_cost: 0
    },
    {
        _id: "natural_pearl",
        name: "Natural Pearl",
        cat: "Trader",
        sub_cat: "Fishing Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "rare_relic",
        name: "Rare Relic",
        cat: "Trader",
        sub_cat: "Excavating Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "shy_wild_flower",
        name: "Shy Wild Flower",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "strong_iron_ore",
        name: "Strong Iron Ore",
        cat: "Trader",
        sub_cat: "Mining Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "sturdy_timber",
        name: "Sturdy Timber",
        cat: "Trader",
        sub_cat: "Logging Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "tender_timber",
        name: "Tender Timber",
        cat: "Trader",
        sub_cat: "Logging Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "timber",
        name: "Timber",
        cat: "Trader",
        sub_cat: "Logging Loot",
        is_material: true,
        price: 0,
        bundle_size: 100,
        stronghold_craft_cost: 0
    },
    {
        _id: "tough_leather",
        name: "Tough Leather",
        cat: "Trader",
        sub_cat: "Hunting Loot",
        is_material: true,
        price: 0,
        bundle_size: 10,
        stronghold_craft_cost: 0
    },
    {
        _id: "wild_flower",
        name: "Wild Flower",
        cat: "Trader",
        sub_cat: "Foraging Rewards",
        is_material: true,
        price: 0,
        bundle_size: 100,
        stronghold_craft_cost: 0
    }
];
const allItems = [...items, ...mats];
export default allItems;