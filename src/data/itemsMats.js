var itemsMats = [
    {
        _id: "adrophine_potion_bright_wild_flower",
        _item_id: "adrophine_potion",
        _mat_id: "bright_wild_flower",
        qty: 6
    },
    {
        _id: "adrophine_potion_rare_relic",
        _item_id: "adrophine_potion",
        _mat_id: "rare_relic",
        qty: 2
    },
    {
        _id: "adrophine_potion_shy_wild_flower",
        _item_id: "adrophine_potion",
        _mat_id: "shy_wild_flower",
        qty: 24
    },
    {
        _id: "adrophine_potion_strong_iron_ore",
        _item_id: "adrophine_potion",
        _mat_id: "strong_iron_ore",
        qty: 2
    },
    {
        _id: "adrophine_potion_wild_flower",
        _item_id: "adrophine_potion",
        _mat_id: "wild_flower",
        qty: 48
    },
    {
        _id: "awakening_potion_crude_mushroom",
        _item_id: "awakening_potion",
        _mat_id: "crude_mushroom",
        qty: 40
    },
    {
        _id: "awakening_potion_exquisite_mushroom",
        _item_id: "awakening_potion",
        _mat_id: "exquisite_mushroom",
        qty: 5
    },
    {
        _id: "awakening_potion_fresh_mushroom",
        _item_id: "awakening_potion",
        _mat_id: "fresh_mushroom",
        qty: 20
    },
    {
        _id: "awakening_potion_rare_relic",
        _item_id: "awakening_potion",
        _mat_id: "rare_relic",
        qty: 4
    },
    {
        _id: "awakening_potion_sturdy_timber",
        _item_id: "awakening_potion",
        _mat_id: "sturdy_timber",
        qty: 2
    },
    {
        _id: "camouflage_robe_crude_mushroom",
        _item_id: "camouflage_robe",
        _mat_id: "crude_mushroom",
        qty: 35
    },
    {
        _id: "camouflage_robe_fresh_mushroom",
        _item_id: "camouflage_robe",
        _mat_id: "fresh_mushroom",
        qty: 22
    },
    {
        _id: "camouflage_robe_tough_leather",
        _item_id: "camouflage_robe",
        _mat_id: "tough_leather",
        qty: 22
    },
    {
        _id: "campfire_exquisite_mushroom",
        _item_id: "campfire",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "campfire_fresh_mushroom",
        _item_id: "campfire",
        _mat_id: "fresh_mushroom",
        qty: 15
    },
    {
        _id: "campfire_timber",
        _item_id: "campfire",
        _mat_id: "timber",
        qty: 12
    },
    {
        _id: "clay_grenade_crude_mushroom",
        _item_id: "clay_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "clay_grenade_exquisite_mushroom",
        _item_id: "clay_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "clay_grenade_fresh_mushroom",
        _item_id: "clay_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "clay_grenade_iron_ore",
        _item_id: "clay_grenade",
        _mat_id: "iron_ore",
        qty: 5
    },
    {
        _id: "corrosive_bomb_crude_mushroom",
        _item_id: "corrosive_bomb",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "corrosive_bomb_exquisite_mushroom",
        _item_id: "corrosive_bomb",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "corrosive_bomb_fresh_mushroom",
        _item_id: "corrosive_bomb",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "corrosive_bomb_heavy_iron_ore",
        _item_id: "corrosive_bomb",
        _mat_id: "heavy_iron_ore",
        qty: 6
    },
    {
        _id: "dark_grenade_crude_mushroom",
        _item_id: "dark_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "dark_grenade_exquisite_mushroom",
        _item_id: "dark_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "dark_grenade_fresh_mushroom",
        _item_id: "dark_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "dark_grenade_tender_timber",
        _item_id: "dark_grenade",
        _mat_id: "tender_timber",
        qty: 3
    },
    {
        _id: "destruction_bomb_crude_mushroom",
        _item_id: "destruction_bomb",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "destruction_bomb_exquisite_mushroom",
        _item_id: "destruction_bomb",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "destruction_bomb_fresh_mushroom",
        _item_id: "destruction_bomb",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "destruction_bomb_heavy_iron_ore",
        _item_id: "destruction_bomb",
        _mat_id: "heavy_iron_ore",
        qty: 6
    },
    {
        _id: "electric_grenade_crude_mushroom",
        _item_id: "electric_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "electric_grenade_exquisite_mushroom",
        _item_id: "electric_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "electric_grenade_fresh_mushroom",
        _item_id: "electric_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "electric_grenade_iron_ore",
        _item_id: "electric_grenade",
        _mat_id: "iron_ore",
        qty: 5
    },
    {
        _id: "elemental_hp_potion_bright_wild_flower",
        _item_id: "elemental_hp_potion",
        _mat_id: "bright_wild_flower",
        qty: 6
    },
    {
        _id: "elemental_hp_potion_shy_wild_flower",
        _item_id: "elemental_hp_potion",
        _mat_id: "shy_wild_flower",
        qty: 24
    },
    {
        _id: "elemental_hp_potion_wild_flower",
        _item_id: "elemental_hp_potion",
        _mat_id: "wild_flower",
        qty: 48
    },
    {
        _id: "flame_grenade_crude_mushroom",
        _item_id: "flame_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "flame_grenade_exquisite_mushroom",
        _item_id: "flame_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "flame_grenade_fresh_mushroom",
        _item_id: "flame_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "flame_grenade_tender_timber",
        _item_id: "flame_grenade",
        _mat_id: "tender_timber",
        qty: 3
    },
    {
        _id: "flare_natural_pearl",
        _item_id: "flare",
        _mat_id: "natural_pearl",
        qty: 20
    },
    {
        _id: "flare_wild_flower",
        _item_id: "flare",
        _mat_id: "wild_flower",
        qty: 35
    },
    {
        _id: "flash_grenade_crude_mushroom",
        _item_id: "flash_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "flash_grenade_exquisite_mushroom",
        _item_id: "flash_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "flash_grenade_fresh_mushroom",
        _item_id: "flash_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "flash_grenade_iron_ore",
        _item_id: "flash_grenade",
        _mat_id: "iron_ore",
        qty: 5
    },
    {
        _id: "frost_grenade_crude_mushroom",
        _item_id: "frost_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "frost_grenade_exquisite_mushroom",
        _item_id: "frost_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "frost_grenade_fresh_mushroom",
        _item_id: "frost_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "frost_grenade_iron_ore",
        _item_id: "frost_grenade",
        _mat_id: "iron_ore",
        qty: 5
    },
    {
        _id: "luterras_horn_bright_wild_flower",
        _item_id: "luterras_horn",
        _mat_id: "bright_wild_flower",
        qty: 4
    },
    {
        _id: "luterras_horn_iron_ore",
        _item_id: "luterras_horn",
        _mat_id: "iron_ore",
        qty: 2
    },
    {
        _id: "luterras_horn_shy_wild_flower",
        _item_id: "luterras_horn",
        _mat_id: "shy_wild_flower",
        qty: 20
    },
    {
        _id: "luterras_horn_strong_iron_ore",
        _item_id: "luterras_horn",
        _mat_id: "strong_iron_ore",
        qty: 2
    },
    {
        _id: "luterras_horn_wild_flower",
        _item_id: "luterras_horn",
        _mat_id: "wild_flower",
        qty: 40
    },
    {
        _id: "marching_flag_crude_mushroom",
        _item_id: "marching_flag",
        _mat_id: "crude_mushroom",
        qty: 38
    },
    {
        _id: "marching_flag_exquisite_mushroom",
        _item_id: "marching_flag",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "marching_flag_natural_pearl",
        _item_id: "marching_flag",
        _mat_id: "natural_pearl",
        qty: 8
    },
    {
        _id: "panacea_crude_mushroom",
        _item_id: "panacea",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "panacea_exquisite_mushroom",
        _item_id: "panacea",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "panacea_fresh_mushroom",
        _item_id: "panacea",
        _mat_id: "fresh_mushroom",
        qty: 16
    },
    {
        _id: "panacea_rare_relic",
        _item_id: "panacea",
        _mat_id: "rare_relic",
        qty: 5
    },
    {
        _id: "pheromone_bomb_crude_mushroom",
        _item_id: "pheromone_bomb",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "pheromone_bomb_exquisite_mushroom",
        _item_id: "pheromone_bomb",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "pheromone_bomb_fresh_mushroom",
        _item_id: "pheromone_bomb",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "pheromone_bomb_heavy_iron_ore",
        _item_id: "pheromone_bomb",
        _mat_id: "heavy_iron_ore",
        qty: 6
    },
    {
        _id: "protective_potion_crude_mushroom",
        _item_id: "protective_potion",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "protective_potion_exquisite_mushroom",
        _item_id: "protective_potion",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "protective_potion_fresh_mushroom",
        _item_id: "protective_potion",
        _mat_id: "fresh_mushroom",
        qty: 16
    },
    {
        _id: "protective_potion_rare_relic",
        _item_id: "protective_potion",
        _mat_id: "rare_relic",
        qty: 5
    },
    {
        _id: "repair_shop_portal_scroll_bright_wild_flower",
        _item_id: "repair_shop_portal_scroll",
        _mat_id: "bright_wild_flower",
        qty: 18
    },
    {
        _id: "repair_shop_portal_scroll_natural_pearl",
        _item_id: "repair_shop_portal_scroll",
        _mat_id: "natural_pearl",
        qty: 16
    },
    {
        _id: "repair_shop_portal_scroll_wild_flower",
        _item_id: "repair_shop_portal_scroll",
        _mat_id: "wild_flower",
        qty: 144
    },
    {
        _id: "sacred_bomb_crude_mushroom",
        _item_id: "sacred_bomb",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "sacred_bomb_exquisite_mushroom",
        _item_id: "sacred_bomb",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "sacred_bomb_fresh_mushroom",
        _item_id: "sacred_bomb",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "sacred_bomb_iron_ore",
        _item_id: "sacred_bomb",
        _mat_id: "iron_ore",
        qty: 3
    },
    {
        _id: "sacred_charm_crude_mushroom",
        _item_id: "sacred_charm",
        _mat_id: "crude_mushroom",
        qty: 30
    },
    {
        _id: "sacred_charm_exquisite_mushroom",
        _item_id: "sacred_charm",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "sacred_charm_fresh_mushroom",
        _item_id: "sacred_charm",
        _mat_id: "fresh_mushroom",
        qty: 18
    },
    {
        _id: "sleep_bomb_crude_mushroom",
        _item_id: "sleep_bomb",
        _mat_id: "crude_mushroom",
        qty: 32
    },
    {
        _id: "sleep_bomb_exquisite_mushroom",
        _item_id: "sleep_bomb",
        _mat_id: "exquisite_mushroom",
        qty: 4
    },
    {
        _id: "sleep_bomb_fresh_mushroom",
        _item_id: "sleep_bomb",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "sleep_bomb_iron_ore",
        _item_id: "sleep_bomb",
        _mat_id: "iron_ore",
        qty: 10
    },
    {
        _id: "stealth_robe_crude_mushroom",
        _item_id: "stealth_robe",
        _mat_id: "crude_mushroom",
        qty: 35
    },
    {
        _id: "stealth_robe_exquisite_mushroom",
        _item_id: "stealth_robe",
        _mat_id: "exquisite_mushroom",
        qty: 5
    },
    {
        _id: "stealth_robe_fresh_mushroom",
        _item_id: "stealth_robe",
        _mat_id: "fresh_mushroom",
        qty: 15
    },
    {
        _id: "stealth_robe_strong_iron_ore",
        _item_id: "stealth_robe",
        _mat_id: "strong_iron_ore",
        qty: 2
    },
    {
        _id: "stealth_robe_tough_leather",
        _item_id: "stealth_robe",
        _mat_id: "tough_leather",
        qty: 2
    },
    {
        _id: "swiftness_robe_shy_wild_flower",
        _item_id: "swiftness_robe",
        _mat_id: "shy_wild_flower",
        qty: 17
    },
    {
        _id: "swiftness_robe_tough_leather",
        _item_id: "swiftness_robe",
        _mat_id: "tough_leather",
        qty: 17
    },
    {
        _id: "swiftness_robe_wild_flower",
        _item_id: "swiftness_robe",
        _mat_id: "wild_flower",
        qty: 27
    },
    {
        _id: "taunting_scarecrow_exquisite_mushroom",
        _item_id: "taunting_scarecrow",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "taunting_scarecrow_fresh_mushroom",
        _item_id: "taunting_scarecrow",
        _mat_id: "fresh_mushroom",
        qty: 15
    },
    {
        _id: "taunting_scarecrow_timber",
        _item_id: "taunting_scarecrow",
        _mat_id: "timber",
        qty: 13
    },
    {
        _id: "thunder_potion_bright_wild_flower",
        _item_id: "thunder_potion",
        _mat_id: "bright_wild_flower",
        qty: 4
    },
    {
        _id: "thunder_potion_rare_relic",
        _item_id: "thunder_potion",
        _mat_id: "rare_relic",
        qty: 5
    },
    {
        _id: "thunder_potion_shy_wild_flower",
        _item_id: "thunder_potion",
        _mat_id: "shy_wild_flower",
        qty: 16
    },
    {
        _id: "thunder_potion_wild_flower",
        _item_id: "thunder_potion",
        _mat_id: "wild_flower",
        qty: 32
    },
    {
        _id: "time_stop_potion_bright_wild_flower",
        _item_id: "time_stop_potion",
        _mat_id: "bright_wild_flower",
        qty: 5
    },
    {
        _id: "time_stop_potion_rare_relic",
        _item_id: "time_stop_potion",
        _mat_id: "rare_relic",
        qty: 2
    },
    {
        _id: "time_stop_potion_shy_wild_flower",
        _item_id: "time_stop_potion",
        _mat_id: "shy_wild_flower",
        qty: 20
    },
    {
        _id: "time_stop_potion_sturdy_timber",
        _item_id: "time_stop_potion",
        _mat_id: "sturdy_timber",
        qty: 2
    },
    {
        _id: "time_stop_potion_wild_flower",
        _item_id: "time_stop_potion",
        _mat_id: "wild_flower",
        qty: 40
    },
    {
        _id: "whirlwind_grenade_crude_mushroom",
        _item_id: "whirlwind_grenade",
        _mat_id: "crude_mushroom",
        qty: 24
    },
    {
        _id: "whirlwind_grenade_exquisite_mushroom",
        _item_id: "whirlwind_grenade",
        _mat_id: "exquisite_mushroom",
        qty: 3
    },
    {
        _id: "whirlwind_grenade_fresh_mushroom",
        _item_id: "whirlwind_grenade",
        _mat_id: "fresh_mushroom",
        qty: 12
    },
    {
        _id: "whirlwind_grenade_tender_timber",
        _item_id: "whirlwind_grenade",
        _mat_id: "tender_timber",
        qty: 3
    }
];
export default itemsMats;