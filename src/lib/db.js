import Datastore from "nedb/browser-version/out/nedb";

class db {
  static tables = {};
  static table(table) {
    if (!(table in this.tables)) {
      this.tables[table] = new Datastore({ filename: table + '.db', autoload: true });
    }
    return this.tables[table];
  }
}

export default db;
