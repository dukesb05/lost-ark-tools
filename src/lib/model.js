import db from "lib/db";

class model {
  static table = "";
  static fields = {};
  static _idField = "_id";

  constructor(data) {
    this.data = data;
    return new Proxy(this, {
      get: function (curModel, field) {
        if (field in curModel.data) {
          return curModel.data[field];
        }
        if (field in curModel) {
          return curModel[field];
        }
        if (field in curModel.constructor && field !== 'name') {
          return curModel.constructor[field];
        }
        return null;
      },
      set: function (curModel, field, value) {
        if (
          Object.keys(curModel.constructor.fields).includes(field) ||
          field === this._idField
        ) {
          curModel.data[field] = value;
        } else {
          curModel[field] = value;
        }
        return true;
      },
    });
  }
  static db() {
    return db;
  }
  load() {
    Object.entries(this.data).forEach(([key, value]) => {
      Object.defineProperty(this, key, {
        value: value,
        writable: true,
      });
    });
  }
  save() {
    if (this.data[this._idField]) {
      return this.update();
    } else {
      return this.create();
    }
  }
  create() {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(base.table).insert(base.data, function (err, newDoc) {
        if (err) reject(err);
        base._id = newDoc._id;
        resolve(true);
      });
    });
  }
  update() {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(base.table).update(
        { _id: base.data[this._idField] },
        base.data,
        {},
        function (err, numReplaced) {
          if (err) reject(err);
          resolve(true);
        }
      );
    });
  }
  delete() {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(base.table).remove(
        { _id: base.data[base._idField] },
        {},
        function (err, numRemoved) {
          if (err) reject(err);
          resolve(true);
        }
      );
    });
  }
  static deleteBy(where) {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(base.table).remove(
        where,
        { multi: true },
        function (err, numRemoved) {
          if (err) reject(err);
          resolve(true);
        }
      );
    });
  }
  static findby(where) {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(this.table).findOne(where, function (err, doc) {
        if (err) reject(err);
        let response;
        if (doc === null) {
          reject(null);
        } else if (doc.length > 1) {
          response = [];
          doc.forEach((element) => {
            response.push(new base(element));
          });
        } else {
          response = new base(doc);
        }
        resolve(response);
      });
    });
  }
  static find(id) {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(this.table).findOne({ _id: id }, function (err, doc) {
        if (err) reject(err);
        let response;
        if (doc === null) {
          reject(null);
        } else if (doc.length > 1) {
          response = [];
          doc.forEach((element) => {
            response.push(new base(element));
          });
        } else {
          response = new base(doc);
        }
        resolve(response);
      });
    });
  }
  static bulkInsert(items) {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(base.table).insert(items, function (err) {
        if (err) reject(err);
        resolve(true);
      });
    });
  }
  static all(options) {
    const base = this;
    return new Promise((resolve, reject) => {
      db.table(this.table).find(options, function (err, doc) {
        if (err) reject(err);
        let response;
        if (doc === null) {
          reject(null);
        } else {
          response = [];
          doc.forEach((element) => {
            response.push(new base(element));
          });
        }
        resolve(response);
      });
    });
  }
  sleep(milliseconds) {
    const date = Date.now();
    const check = milliseconds + date;
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < check);
  }

  static wrapPromise(promise) {
    let status = "pending";
    let response;

    const suspender = promise.then(
      (res) => {
        status = "success";
        if (res === null) {
          response = null;
        } else if (res.length > 1) {
          response = [];
          res.forEach((element) => {
            response.push(new this(element));
          });
        } else {
          response = new this(res);
        }
      },
      (err) => {
        status = "error";
        response = err;
      }
    );

    const read = () => {
      switch (status) {
        case "pending":
          throw suspender;
        case "error":
          throw response;
        default:
          return response;
      }
    };

    return { read };
  }
  toJSON() {
    return this.data;
  }
}

export default model;
