import model from "lib/model";
import itemsMatsModel from "lib/models/itemsMats";

class items extends model {
    static table = "items";
    static fields = {
        _id: { required: false },
        name: { required: true },
        cat: { required: true },
        sub_cat: { required: true },
        is_material: { required: true },
        price: { required: true },
        bundle_size: { required: false },
        stronghold_craft_cost: { required: false }
    };
    static newFromAe(newData) {
        let options = {};
        if (typeof newData._id !== 'undefined' && newData._id) {
            options._id = newData._id;
        }
        let item = new this(options);

        Object.keys(this.fields).forEach(field => {
            if (newData.hasOwnProperty(field)) {
                item[field] = newData[field];
            }
        });
        if (item.hasOwnProperty('is_material') && item.is_material !== '') {
            item.is_material = item.is_material === true || item.is_material === 1;
            if (item.is_material === true) {
                item.stronghold_craft_cost = 0;
            } else if (item.is_material === false) {
                item.bundle_size = 1;
            }
        }
        if (item.price !== null) {
            item.price = parseInt(item.price);
        }
        if (item.bundle_size !== null && item.bundle_size !== '') {
            item.bundle_size = parseInt(item.bundle_size);
        }
        if (item.stronghold_craft_cost !== null && item.stronghold_craft_cost !== '') {
            item.stronghold_craft_cost = parseInt(item.stronghold_craft_cost);
        }
        return item;
    }
    valid() {
        const base = this;
        return new Promise((resolve, reject) => {
            var errors = [];
            let errMsg = "Please fill in all empty fields";
            if (base.is_material === '') {
                base.fields.bundle_size.required = false;
                base.fields.stronghold_craft_cost.required = false;
            } else if (base.is_material === true) {
                base.fields.bundle_size.required = true;
                base.fields.stronghold_craft_cost.required = false;
            } else if (base.is_material === false) {
                base.fields.bundle_size.required = false;
                base.fields.stronghold_craft_cost.required = true;
            }
            Object.keys(base.fields).forEach(field => {
                if (base.fields[field].required && (typeof base.data[field] === 'undefined' || !base.data[field])) {
                    if (field === 'is_material' && base.data[field] === false) {
                        return;
                    }
                    errors.push(field);
                }
            });
            if ((base.price <= 0 || isNaN(base.price)) && base.price !== null) {
                errors.push('price');
                errMsg += "\nPlease enter a valid price";
            }
            if ((base.bundle_size <= 0 || isNaN(base.bundle_size)) && base.bundle_size !== '' && base.bundle_size !== null) {
                errors.push('bundle_size');
                errMsg += "\nPlease enter a valid bundle size";
            }
            if ((base.stronghold_craft_cost <= 0 || isNaN(base.stronghold_craft_cost)) && base.stronghold_craft_cost !== '' && base.stronghold_craft_cost !== null && base.stronghold_craft_cost !== 0) {
                errors.push('stronghold_craft_cost');
                errMsg += "\nPlease enter a valid stronghold craft cost";
            }
            if (errors.length > 0) {
                reject({ errors: errors, msg: errMsg });
            } else {
                resolve(true);
            }
        });
    }
    static craftingTableData(key = 'items', sort_key = 'profit', sort_dir = 'desc') {
        const base = this;
        return new Promise((resolve, reject) => {
            base.all().then((items) => {
                itemsMatsModel.all().then((itemsMats) => {
                    let data = { items: [], mats: [] };
                    items.forEach(item => {
                        if (item.is_material) {
                            data.mats[item._id] = item;
                        } else {
                            data.items[item._id] = item;
                            data.items[item._id].mats = [];
                        }
                    });
                    itemsMats.forEach(itemMat => {
                        if (data.items.hasOwnProperty(itemMat._item_id)) {
                            data.items[itemMat._item_id].mats[itemMat._mat_id] = { ...itemMat, ...data.mats[itemMat._mat_id].toJSON() };
                            data.items[itemMat._item_id].mats[itemMat._mat_id].qty = itemMat.qty;
                        }
                    });
                    for (const [item_id, item] of Object.entries(data.items)) {
                        let cost = 0;
                        for (const [mat_id, mat] of Object.entries(item.mats)) {
                            const matBundleSize = data.items[item_id].mats[mat_id].bundle_size;

                            data.items[item_id].mats[mat_id].unit_price = Number.parseFloat((
                                Math.round(
                                    (data.items[item_id].mats[mat_id].price / matBundleSize +
                                        Number.EPSILON) *
                                    100
                                ) / 100
                            ).toFixed(2));
                            data.items[item_id].mats[mat_id].qty_per_craft = Number.parseFloat((
                                Math.round(
                                    (mat.qty / 3 +
                                        Number.EPSILON) *
                                    100
                                ) / 100
                            ).toFixed(2));
                            data.items[item_id].mats[mat_id].total_cost = Number.parseFloat((
                                Math.round(
                                    (data.items[item_id].mats[mat_id].unit_price * data.items[item_id].mats[mat_id].qty_per_craft +
                                        Number.EPSILON) *
                                    100
                                ) / 100
                            ).toFixed(2));
                            cost += data.items[item_id].mats[mat_id].total_cost;
                        }
                        data.items[item_id].crafting_cost = Number.parseFloat((
                            Math.round(
                                ((cost) + (data.items[item_id].stronghold_craft_cost) +
                                    Number.EPSILON) *
                                100
                            ) / 100
                        ).toFixed(2));
                        data.items[item_id].profit = Number.parseFloat((data.items[item_id].price - data.items[item_id].crafting_cost).toFixed(2));
                    }
                    let result = {};
                    switch (key) {
                        case 'both':
                            result.items = Object.values(data.items);
                            result.items.sort((a, b) => (a[sort_key] > b[sort_key] ? (sort_dir === 'desc' ? -1 : 1) : (sort_dir === 'desc' ? 1 : -1)));
                            result.mats = Object.values(data.mats);
                            result.mats.sort((a, b) => (a[sort_key] > b[sort_key] ? (sort_dir === 'desc' ? -1 : 1) : (sort_dir === 'desc' ? 1 : -1)));
                            resolve(result);
                            break;
                        case 'materials':
                            result = Object.values(data.mats);
                            result.sort((a, b) => (a[sort_key] > b[sort_key] ? (sort_dir === 'desc' ? -1 : 1) : (sort_dir === 'desc' ? 1 : -1)));
                            resolve(result);
                            break;
                        default:
                            result = Object.values(data.items);
                            result.sort((a, b) => (a[sort_key] > b[sort_key] ? (sort_dir === 'desc' ? -1 : 1) : (sort_dir === 'desc' ? 1 : -1)));
                            resolve(result);
                    }
                })
            });
        });
    }
}
export default items;
