import model from "lib/model";
class itemsMats extends model {
  static table = "itemsMats";
  static fields = {
    _id: { required: true },
    _item_id: { required: true },
    _mat_id: { required: true },
    qty: { required: true }
  };
}
export default itemsMats;
