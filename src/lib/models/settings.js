import model from "lib/model";
class settings extends model {
  static table = "settings";
  static fields = {
    _id: { required: true },
    db_version: { required: true }
  };
  static inst() {
    const base = this;
    return new Promise((resolve, reject) => {
      base.find(1).then(
        (res) => {
          resolve(res);
        },
        () => {
          const defaultSettings = {
            _id: 1,
            db_version: 0
          };
          const newSettings = new base(defaultSettings);
          newSettings.create().then(() => {
            resolve(newSettings);
          }, (err) => { reject(err) })
        }
      );
    });
  }
}
export default settings;
