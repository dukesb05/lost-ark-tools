const categories = {
    "Combat Supplies": [
        "Battle Item - Recovery",
        "Battle Item - Offense",
        "Battle Item - Utility",
        "Battle Item - Buff",
    ],
    Cooking: ["All"],
    Trader: [
        "Foraging Rewards",
        "Logging Loot",
        "Mining Loot",
        "Hunting Loot",
        "Fishing Loot",
        "Excavating Loot",
        "Other",
    ],
};
export default categories;