const showAeModal = (state, item, itemMats) => {
    state.item.setData(item);
    state.mats.setData(itemMats);
    state.show.setData(true);
};
export { showAeModal };