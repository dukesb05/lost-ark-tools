import { atom } from "recoil";
import { useState, useEffect } from 'react';

import itemsModel from "lib/models/items";
import settings from "lib/models/settings";

let state = {};
state.component = {
    numberField: {
        interval: atom({
            key: "component.numberField.interval",
            default: null,
        }),
        sliderMouseDown: atom({
            key: "component.numberField.sliderMouseDown",
            default: false,
        }),
        sliderX: atom({
            key: "component.numberField.sliderX",
            default: false,
        })
    },
    aeItem: {
        show: atom({
            key: "component.aeItem.show",
            default: false,
        }),
        submitting: atom({
            key: "component.aeItem.submitting",
            default: false,
        }),
        item: atom({
            key: "component.aeItem.item",
            default: {},
        }),
        mats: atom({
            key: "component.aeItem.mats",
            default: [],
        }),
        errors: atom({
            key: "component.aeItem.errors",
            default: null,
        }),
        errMsg: atom({
            key: "component.aeItem.errMsg",
            default: {},
        }),
        fieldsChecked: atom({
            key: "component.aeItem.fieldsChecked",
            default: [],
        }),
        saveSuccessful: atom({
            key: "component.aeItem.saveSuccessful",
            default: false,
        }),
    }
}

state.page = {
    updatePrices: {
        updates: atom({
            key: "pages.updatePrices.updates",
            default: {},
        }),
        delModalShow: atom({
            key: "pages.updatePrices.delModalShow",
            default: false,
        }),
        delModalItem: atom({
            key: "pages.updatePrices.delModalItem",
            default: {},
        }),
        forceItemsReload: atom({
            key: "pages.updatePrices.forceItemsReload",
            default: 0,
        }),
    },
    craftingProfit: {
        forceItemsReload: atom({
            key: "pages.craftingProfit.forceItemsReload",
            default: 0,
        }),
    }
}

function useBuildLocal(recoilFunc, keys) {
    let localState = {};
    Object.keys(keys).forEach(mainKey => {
        localState[mainKey] = {};
        let subKeys = [];
        if (keys[mainKey].constructor === Array) {
            subKeys = keys[mainKey];
        } else {
            subKeys = Object.keys(keys[mainKey]);
        }
        subKeys.forEach(subKey => {
            localState[mainKey][subKey] = {};
            Object.keys(state[mainKey][subKey]).forEach(field => {
                const fields = keys[mainKey].constructor === Array ? false : keys[mainKey][subKey];
                if (fields === false || fields.includes(field)) {
                    const [data, setFunc] = recoilFunc(state[mainKey][subKey][field]);
                    localState[mainKey][subKey][field] = {};
                    localState[mainKey][subKey][field].data = data;
                    localState[mainKey][subKey][field].setData = setFunc;
                }
            });
        });
    });
    return localState;

}
state.buildLocal = useBuildLocal;
const useGetItems = (key = 'items', sort_key = 'profit', sort_dir = 'desc', reloadNum = 0) => {
    const [Items, setItems] = useState(null);
    useEffect(() => {
        function handleStatusChange(status) {
            setItems(status);
        }
        itemsModel.craftingTableData(key, sort_key, sort_dir).then(
            (res) => {
                handleStatusChange(res);
            },
            () => {
                handleStatusChange(false);
            }
        );
    }, [key, sort_key, sort_dir, reloadNum]);

    return Items;
}
state.getItems = useGetItems;
const curDbVersion = 1.0;
const useIsSetup = () => {
    const [isSetup, setIsSetup] = useState(false);
    useEffect(() => {
        function handleStatusChange(status) {
            setIsSetup(status);
        }
        settings.inst().then(
            (res) => {
                if (res.db_version >= curDbVersion) {
                    handleStatusChange(true);
                } else {
                    handleStatusChange(false);
                }
            },
            () => {
                handleStatusChange(false);
            }
        );
    }, []);

    return isSetup;
}
state.isSetup = useIsSetup;
export default state;